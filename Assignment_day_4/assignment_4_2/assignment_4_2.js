/*global console:true, require:true */
var fs = require('fs');
var obj = require('./lib.js').obj;

var temp1;
var temp2 = 3;
var temp3 = 'string';
var temp4 = null;
var temp5 = true;
var temp6 = function (){};
var temp7 = {a:1};
var temp8 = [1,2,3];

var array = [temp1,temp2,temp3,temp4,temp5,temp6,temp7,temp8];
var i;

for(i in array) {
    console.log("if the input is "+ typeof(array[i]));
    console.log("isUndefined : " + obj.isUndefined(array[i]));
    console.log("isNan : " + obj.isNan(array[i]));
    console.log("isNumber : " + obj.isNumber(array[i]));
    console.log("isString : " + obj.isString(array[i]));
    console.log("isBoolean : " + obj.isBoolean(array[i]));
    console.log("isFunction : " + obj.isFunction(array[i]));
    console.log("isObject : " + obj.isObject(array[i]));
    console.log("isarray : " + obj.isarray(array[i]));
    console.log();
}

fs.readFile('demo',function(err,data){
    if(obj.isError(err)){
        console.log("Error");
    } else {
        console.log("Not an error");
    }
});
