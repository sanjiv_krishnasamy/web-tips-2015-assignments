/*global exports:true*/
var obj = {};
obj.isUndefined = function (value) {
    if(value){
        return false;
    } else {
        return true;
    }
};
obj.isNull = function (value) {
    if (value === null) {
        return true;
    } else {
        return false;
    }
};
obj.isNan= function (value) {
    if(typeof(value) === 'number') {
        return false;
    } else {
        return true;
    }
};
obj.isError = function (value) {
    if(value.hasOwnProperty('errno')) {
        return true;
    } else {
        return false;
    }
};
obj.isBoolean = function (value) {
    if(typeof(value) === 'boolean') {
        return true;
    } else {
        return false;
    }
};
obj.isNumber = function (value) {
    if(typeof(value) === 'number') {
        return true;
    } else {
        return false;
    }
};
obj.isString = function (value) {
    if(typeof(value) === "string") {
        return true;
    } else {
        return false;
    }
};
obj.isFunction = function (value) {
    if(typeof(value) === 'function') {
        return true;
    } else {
        return false;
    }
};
obj.isObject = function (value) {
    if(typeof(value) === 'object') {
        return true;
    } else {
        return false;
    }
};
obj.isarray = function (value) {
    if(Array.isArray(value)) {
        return true;
    } else {
        return false;
    }
};
exports.obj = obj;