/*global console:true, require:true, process:true, describe:true, it:true */
var assert = require("assert");
var obj = require('./lib.js').obj;

//Test case for isUndefined
describe('isUndefined', function(){
    it('should return true if the input is Undefined', function(){
      var a;
      assert.equal(true, obj.isUndefined(a));
      assert.equal(false, obj.isUndefined(a=2));
    });
});
//Test case for isNull
describe('isNull', function(){
    it('should return true if the input is null', function(){
      assert.equal(true, obj.isNull(null));
      assert.equal(false, obj.isNull(3));
    });
});
//Test case for isNan
describe('isNan', function(){
    it('should return true if the input is not a number', function(){
      assert.equal(true, obj.isNan('a'));
      assert.equal(false, obj.isNan(4));
      assert.equal(false, obj.isNan(-4));
    });
});
//Test case for isBoolean
describe('isBoolean', function(){
    it('should return true if the input is Boolean', function(){
      assert.equal(true, obj.isBoolean(false));
      assert.equal(false, obj.isBoolean('true'));
    });
});
//Test case for isNumber
describe('isNumber', function(){
    it('should return true if the input is number', function(){
      assert.equal(true, obj.isNumber(3));
      assert.equal(false, obj.isNumber('3'));
    });
});
//Test case for isString
describe('isString', function(){
    it('should return true if the input is string', function(){
      assert.equal(true, obj.isString('2'));
      assert.equal(false, obj.isString(2));
    });
});
//Test case for isFunction
describe('isFunction', function(){
    it('should return true if the input is function', function(){
      assert.equal(true, obj.isFunction(function(){}));
      assert.equal(false, obj.isFunction(3));
    });
});
//Test case for isObject
describe('isObject', function(){
    it('should return true if the input is object', function(){
      assert.equal(true, obj.isObject({a:1}));
      assert.equal(false, obj.isObject('a'));
    });
});
//Test case for isarray
describe('isarray', function(){
    it('should return true if the input is Array', function(){
      assert.equal(true, obj.isarray([]));
      assert.equal(false, obj.isarray({}));
    });
});