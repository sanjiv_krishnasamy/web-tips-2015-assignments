#! /usr/bin/env node
var Table = require('cli-table');
var cal = new Date();

console.log("Today's Date : "+cal.getDate() +'-'+ cal.getMonth() +'-'+ cal.getFullYear());
console.log('current time : '+cal.getHours() +' : '+cal.getMinutes() +' : '+ cal.getSeconds());
var split = new Date().toString().split(" ");
var timeZoneFormatted = '';
for (var i =6; i<split.length; i++){
    timeZoneFormatted += split[i] + ' ';
}
console.log("Time Zone : "+timeZoneFormatted);

var month = cal.getMonth();
var year = cal.getFullYear();

// date object with first day of current month
var dt=new Date(year, month, 01);

// first day of present month ( from 0 to 6 )
var first_day=dt.getDay();

// Set to next month and one day backward. 
dt.setMonth(month+1,0);

// Last date of present month
var last_date=dt.getDate(); 

var day = 1;
var string = '';
for(i=0; i<42; i++){
    if((i>= first_day) && (day<= last_date)){
        string += day+',';
        //console.log(dy);
        day++;
    } else {
        string += " ,";
        //console.log("*");
    }
}

var array = string.split(',');
 //Generating the table
var table = new Table({
    head: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    colWidths: [10, 10, 10, 10, 10, 10, 10]
});
for (var i=0; i<42; i++) {
    if(i%7 === 0){
        table.push([array[i],array[i+1],array[i+2],array[i+3],array[i+4],array[i+5],array[i+6]]);
    }
}
console.log(table.toString());