//Find the size of a folder by adding up the file sizes of all the files in the folder? Get the folder path as an argument
/*global console:true, require:true, process:true */

var fs = require("fs");

//Reading a file from the folder
fs.readdir('assignment_3_1_files/',function (err, files) {
    var i, count =0;
    var totalSize =0;
    if (err) {
        throw err;
    }
    
    //Function to print the Total size of the files in the specified folder
    function printme() {
        console.log ("Total size of the folder - assignment_3_1_files is "+totalSize+" bytes");
    }
    
    //Loop to add the size of the files
    for(i=0; i<files.length; i++) {
        fs.stat('assignment_3_1_files/'+files[i],function (err2, file) {
            if (err2) {
                console.log(err2);
            }
            count++;
            totalSize += parseInt(file.size);
            if (count === files.length){
                printme();
            }
        });
    }
});