//Repeat 3rd question with synchronous file operations. Compare the time taken with the async one and give your comments on the same.
/*global console:true, require:true, process:true */

var fs = require('fs');
var exec = require('child_process').exec;

var array = [];
var sourceDestination = [];
var i;
var j;
var start = new Date().getTime();
//Read the config file
var data = fs.readFileSync('config.txt');
var data1 = data.toString();
array = data1.split('"');
for (i=0; i<array.length; i++) {
    if (i%2 == 1) {
        sourceDestination.push(array[i]);
    }
}

//This function performs the copy operation
function InitiateCopy(i) {
    
    //Checking whether source directory or a file
	if(fs.lstatSync(sourceDestination[i]).isDirectory()) {
        //Reading the files from that directory
		var files = fs.readdirSync(sourceDestination[i]);
        console.log(files);
            for(j=0;j<files.length;j++) {
                if(fs.existsSync(sourceDestination[i+1]+'\\'+files[j])===0) {
                    exec('COPY '+sourceDestination[i]+'\\'+files[j]+" "+sourceDestination[i+1]);
                }
            }
	} else {
		exec('COPY '+sourceDestination[i]+" "+sourceDestination[i+1]);
	}
}
//Now sourcedestination array contains the source and destination 
for(i=0;i<sourceDestination.length;i++) {	
    if(i%2 === 0) {
        if(!fs.existsSync(sourceDestination[i+1])) {
            exec('MKDIR '+sourceDestination[i+1]);
        }
        if(fs.existsSync(sourceDestination[i])===0) {	
            continue;
        }
        console.log(sourceDestination[i],sourceDestination[i+1]);
        InitiateCopy(i);
    }
}	
var end = new Date().getTime();
var time = end - start;
var append = fs.appendFileSync('config.txt','Time Taken by Sync method :'+time+' ms');