//Write a program that clears all the destination files that have been copied. Use the same config file to find the copied files and note that you should not delete any other files/folders other than the copied ones.
/*global console:true, require:true, process:true */

var fs = require('fs');
var exec = require('child_process').exec;

var array = [];
var sourceDestination = [];
var i;
var j;
//Read the config file
fs.readFile('config.txt',function(err,data){
    var data1 = data.toString();
    array = data1.split('"');
        for (i=0; i<array.length; i++) {
            if (i%2 == 1) {
                sourceDestination.push(array[i]);
            }
        }
    //Now sourcedestination array contains the source and destination 
    for(i=0;i<sourceDestination.length;i++) {	
        if(i%2 == 1) {
            DeleteDest(i);
        }	
    }
});

//This function performs the copy operation
function DeleteDest(i) {
    exec("rmdir /Q /S "+sourceDestination[i]);
}