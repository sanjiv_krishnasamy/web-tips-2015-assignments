//Extend the second question. Write the time taken to copy each file, in the same config file.
/*global console:true, require:true, process:true */

var fs = require('fs');
var exec = require('child_process').exec;

var array = [];
var sourceDestination = [];
var i;
var j;
var count=0;

var start = new Date().getTime();
//Read the config file
fs.readFile('config.txt',function(err,data){
    var data1 = data.toString();
    array = data1.split('"');
        for (i=0; i<array.length; i++) {
            if (i%2 == 1) {
                sourceDestination.push(array[i]);
            }
        }
    
    //Now sourcedestination array contains the source and destination 
    for(i=0;i<sourceDestination.length;i++) {	
        if(i%2 === 0) {
            CheckPath(i);
        }	
    }
});

//Checks whether destination directory exists. If not it creates one
function CheckPath (i) {
    fs.exists(sourceDestination[i+1], function (exists) {
        if (!exists) {
            fs.mkdir(sourceDestination[i+1],function (error){
                if (error) {
                    console.log("Destination Directory Already exists..!!!");
                } else {
                    console.log("Successfully created new Destination directory");
                }
            });
        }
        //Checks whether source file exists
        fs.exists(sourceDestination[i], function (exists) {	
            if(!exists) {
                console.log("source file missing");
            } else {
                console.log("i"+i);
                InitiateCopy(i);
            }
        });
    });
}

//Copies the source file to the destination
function CopyFiles (source, destination, file) {
    fs.exists(destination+'\\'+file, function (exists) {
        if(!exists) {
            exec('COPY '+source+'\\'+file+" "+destination);
            count++;
            StopTime(count);
        }
    });
}
//This function performs the copy operation
function InitiateCopy(i) {
    
    //Checking whether source directory or a file
	fs.stat(sourceDestination[i], function (err, stats) {
        if(stats.isDirectory()) {
            //Reading the files from that directory
            fs.readdir(sourceDestination[i],function(err,files){
                for(j=0;j<files.length;j++) {
                    CopyFiles(sourceDestination[i],sourceDestination[i+1],files[j]);
                }
            });
        } else {
            exec('COPY '+sourceDestination[i]+" "+sourceDestination[i+1]);
            count ++;
            StopTime(count);
        }
    }); 
}
function StopTime (count) {
var end = new Date().getTime();
    var time = end - start;
    fs.appendFile('config.txt','Time Taken by Async method to copy '+count+' file is :'+time+' ms',function (err) {
        if(err) {
            console.log(err);
        }
    });
}