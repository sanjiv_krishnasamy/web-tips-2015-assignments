* Assignment Questions :

1_a : Print a diamond shape in console using '*'. No. of lines should be configurable

1_b : Create a Fibonacci. Series length should be configurable

1_c : Reverse a string after removing the vowels and spaces from the given sentence

1_d : Create an array of student objects. Each object should contain id, age, name etc. Sort based on the id

1_e : Create a Object to contain authors and their works. Have the parameter to represent author name and the book names as part of values. Create functions to add new books, new author and search based on book names (partial field is also expected)

2_1 :Describe camera as an object and define some of its characteristics and operations.

2_2 :Use for loop and enumerate the camera object and display in console.log

2_3 :Use ‘typeof’ in the above enumeration and limit characteristics of type only ‘Number’

2_4 :Create an object for soliton training room. Whatever and whoever is present at that moment should be a property of that object and assign a value which is relevant to the property

2_5 :Create arithmetic functions - add, subtract, multiply and divide which takes two ‘number’ arguments and return the result. Implement it using “Method” invocation

2_6 :Implement the above using “Function” invocation

2_7 :Implement the above using “Constructor” invocation

2_8 :Implement the above using “apply” invocation

2_9 :Implement the arithmetic module using “closure” Expose function to set their value

2_10 :Check whether the memory can be changed through any other means in the case of closure. State your experiments 

2_11: Line, Rectangle, Square and Rhombus. Provide an inheritance relation based on you feel (Should be multiple and multi level) and do calculate Area. Do this implementation in all three methods of inheritance

2_12 :Extend the Number, String etc functionality with your own functions. Choose one or two functions for each type. Use functional inheritance

2_13 :Create an array of things that you are interested in life.

2_14 :Use for loop and enumerate through your items and print in console for the above two arrays

2_15 :Can you update the length property by yourself? If so then share more insights on that.

2_16 :Difference between array and objects

2_17 :Can you update the length property by yourself? If so then share more insights on that.

2_18 :Reverse the string “This is TIPS 2015” using reverse function.

2_19 :Write a “stack” using Arrays?