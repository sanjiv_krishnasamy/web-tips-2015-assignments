/*global console:true, require:true, process:true, exports:true */

var inquirer = require("inquirer");
var Table = require('cli-table');

//Initial function to be called to iterate the user's choice
function BeginFromFirst () {
    
    //Getting a choice from the user
    console.log("1.Diamond Generation \n2.Fibonacci series\n3.Removing vowels and space and Reversing the sentence \n4.Array Sorting \n5.Book Library \n6.Quit");
    var choice = [
        {
            type: 'input',
            name: 'number',
            message: 'Enter your choice :'
        }
    ];
    inquirer.prompt(choice, function( choice ) {
        
        //Switching the function based on the user's choice
        switch (parseInt(choice.number)) {
            case 1:
                GenerateDiamond();
                break;
            case 2:
                GenerateFibonacci();
                break;
            case 3:
                ReverseSentence();
                break;
            case 4:
                SortArray();
                BeginFromFirst ();
                break;
            case 5:
                BookSearch();
                break;
            case 6:
                console.log("\nTHANKS FOR USING ME");
                process.exit();
                break;
            default:
                console.log("\nInvalid Choice");  
                BeginFromFirst ();
        }
    });
}
//If choice is 1, it switches to diamond generation
function GenerateDiamond() {
    var input = [
        {
            type: 'input',
            name: 'number',
            message: 'Enter a number of lines for a DIAMOND:'
        }
    ];
    inquirer.prompt(input, function( answers ) {
        try {
            if (!isNaN(answers.number)) {
                if(answers.number > 0) {
                    Diamond(answers.number);
                } else {
                    throw "Enter a number greater than zero";
                }
            } else {
                throw "Not a valid number";
            }
        } catch (e) {
            console.log(e);
            GenerateDiamond();
        }
    });
}
//Generates the diamond for the input
function Diamond(n) {
    try {
        if (n % 2 === 0 )   {
            throw "You cannot construct a DIAMOND in even number of lines.Please enter any ODD number";
        } else {
            PrintDiamond (n);
            BeginFromFirst ();
        }
    }
    catch (e){
        console.log(e);
        GenerateDiamond();
    }
}

var PrintDiamond = function (n){
    var i,j,s;
    //Top of the diamond
    for(i=1; i<=n; i+=2) {
        for (s=0; s<(n/2-i/2); s++) { 
            process.stdout.write(" ");
        }
        for (j=1; j<=i; j++) {
            process.stdout.write("*");
        }
        process.stdout.write("\n");
    }

    // bottom of the diamond
    for (i=n-2; i>=0; i=i-2) {
        for (s=0; s<(n/2-i/2); s++) {
            process.stdout.write(" ");
        }
        for (j=1; j<=i; j++) {
            process.stdout.write("*");
        }
        process.stdout.write("\n");
    }
};

//If choice is 2, it switches to Fibonacci generation
function GenerateFibonacci () {
    var input = [
        {
            type: 'input',
            name: 'number',
            message: 'Enter a Length for a Fibonacci:'
        }
    ];
    inquirer.prompt(input, function( answers ) {
        try{
            if (!isNaN(answers.number)) {
                if (answers.number > 0) {
                    console.log("Fibonacci Series :");
                    Fibonacci(answers.number);
                    BeginFromFirst ();
                } else {
                    throw "Enter a number greater than zero";
                }
            } else {
                throw "Not a valid number";
            }
        } catch (e) {
            console.log(e);
            GenerateFibonacci();
        }
    });
}
//Function to generate fibonacci
var Fibonacci = function (number) {
    var first = 0;
    var second = 1;
    var next;
    var series =[];
    var i;
    for (i=0; i<number; i++ ) {
        if (i <= 1) {
            next = i;
        } else {
            next = first + second;
            first = second;
            second = next;
        }
        process.stdout.write(" "+next);
        series.push(next);
    }
    process.stdout.write("\n");
    return series;
};

//If choice is 3, it asks the user to enter the sentence
function ReverseSentence() {
    var input = [
        {
            type: 'input',
            name: 'sentence',
            message: 'Enter a sentence:'
        }
    ];
    inquirer.prompt(input, function( answers ) {
        console.log("Resulting Sentence :");
        ReverseMe(answers.sentence);
        BeginFromFirst ();
    });
}
//Performing the Reverse operation
var ReverseMe = function(sentence) {
    var newString = "";
    var i;
    var resultingSentence = sentence.split(" "); //resultingSentence contains the array of words

    resultingSentence = resultingSentence.join(""); //Words are joined and resultingSentence became string
    resultingSentence = resultingSentence.replace(/[aeiou]/gi, "");//Replacing aeiou with null

    //Reversing the string
    for (i = resultingSentence.length; i > 0; i--) {
        newString += resultingSentence[i-1];
    }
    console.log(newString);
    process.stdout.write("\n");
    return newString;
};

//If choice is 4, it performs sorting in the array pf objects
function SortArray () {
    var studentCollection = [
        {id : 1, name : "Sanjiv", age : 18},
        {id : 3, name : "Vignesh", age : 17},
        {id : 2, name : "Shylesh", age : 20}
    ];
    var i;
    console.log ("Before sorting");
    
    //Before sorting
    var table = new Table({
        head: ['ID', 'Name', 'Age'],
        colWidths: [20, 20, 20]
    });
    for (i in studentCollection ) {
        table.push([studentCollection[i].id, studentCollection[i].name, studentCollection[i].age]);
    }
    console.log(table.toString());
    
    //Inbuilt sort that calls compare function
    studentCollection.sort(Compare);

    console.log ("After sorting");
    //Printing the student collection
        table = new Table({
        head: ['ID', 'Name', 'Age'],
        colWidths: [20, 20, 20]
    });
    for (i in studentCollection ) {
        table.push([studentCollection[i].id, studentCollection[i].name, studentCollection[i].age]);
    }
    console.log(table.toString());
}

//Sorts by comparing the id of the object
var Compare = function (element1, element2) {
    if (element1.id < element2.id) {
        return -1;
    } else {
        return 1;
    }
};

//if choice is 5, it enters the book library function
function BookSearch () {
    var bookCollection = [
        {
            author : "Gandhi",
            book : "My Experiments with Truth"
        },
        {
            author : "William shakespeare",
            book : "The Merchant of venice"
        },
        {
            author : "Thomas Hardy",
            book : "Far from the Madding Crowd"
        },
        {
            author : "Rabindra Nath Tagore",
            book : "Geetanjali"
        }
    ];
    RunBookSearch(bookCollection);
}
//Getting the user's choice of operation to be performed 
function RunBookSearch (bookCollection) {
    console.log ("Choose the option : \n1.Add a Book \n2.Search for a Book \n3.View Books \n4.Exit");
    var input = [
        {
            type: 'input',
            name: 'option',
            message: 'Enter your option :'
        }
    ];
    inquirer.prompt(input, function( answers ) {
        if (!isNaN(answers.option)) {
            switch (parseInt(answers.option)) {
                case 1:
                    AddBook(bookCollection);
                    break;
                case 2:
                    SearchBook(bookCollection);
                    break;
                case 3:
                    ViewBooks(bookCollection);
                    break;
                case 4:
                    BeginFromFirst();
                    break;
                default:
                    console.log("Invalid Choice !!!");
                    BookSearch();
            }
        } else {
            console.log("Not a valid number");
            BookSearch();
        }
    });
}
//Adding the new book to the library
function AddBook (bookCollection) {
    var bookDetails = [
        {
            type: 'input',
            name: 'bookName',
            message: 'Enter the name of the Book:'
        },
        {
            type: 'input',
            name: 'authorName',
            message: 'Enter the name of the Author:'
        }
    ];
    inquirer.prompt(bookDetails, function( answers ) {
        try {
            if(answers.authorName.length > 0 && answers.bookName.length > 0) {
                var newBook = {
                    author : answers.authorName,
                    book : answers.bookName
                };
                bookCollection.push(newBook);
                console.log ("Successfully Added !!!");
                RunBookSearch(bookCollection);
            } else {
                throw "Empty Field is unacceptable !!!";
            }
        } catch(e) {
            console.log(e);    
            AddBook (bookCollection);
        }
    });
}

//Searching the book from the library
function SearchBook (bookCollection) {
    var bookDetails = [
        {
            type: 'input',
            name: 'bookName',
            message: 'Enter a name of a book to be searched :'
        }
    ];
    inquirer.prompt(bookDetails, function(answers) {
        var i;
        var bookToBeSearched = answers.bookName.toLowerCase();
        var searchResult = [];
        
        //Storing the search results in searchResult
        for (i in bookCollection) {
            if(bookCollection[i].book.toLowerCase().indexOf(bookToBeSearched) >= 0) {
                searchResult.push(bookCollection[i]);
            }
        }
        
        //If no book name matches with the search text
        if(searchResult.length > 0) {
            console.log("Book Found !!!");

            //Generating the table
            var table = new Table({
                head: ['Book Name', 'Author Name'],
                colWidths: [30, 30]
            });
            for (i in searchResult ) {
                table.push([searchResult[i].book, searchResult[i].author]);
            }
            console.log(table.toString());
        } else {
            console.log("No Results found !!!");
        }
        RunBookSearch (bookCollection);
    });
}

//Displaying the details of the book available in the library
function ViewBooks (bookCollection) {
    var table = new Table({
        head: ['Book Name', 'Author Name'],
        colWidths: [30, 30]
    });
    for (var i in bookCollection ) {
        table.push([bookCollection[i].book, bookCollection[i].author]);
    }
    console.log(table.toString());
    RunBookSearch (bookCollection);
}

//Initiating the Function
BeginFromFirst ();
exports.Fibonacci = Fibonacci;
exports.PrintDiamond = PrintDiamond;
exports.ReverseMe = ReverseMe;
exports.Compare = Compare;