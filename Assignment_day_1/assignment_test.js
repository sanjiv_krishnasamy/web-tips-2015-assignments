/*global console:true, require:true, process:true, describe:true, it:true */
var assert = require("assert");
var mainObject = require("./assignment");

//Test case for generating a fibonacci series by passing an argument
describe('Fibonacci Series', function(){
    it('should return Fibonacci series', function(){
      assert.deepEqual([0,1,1,2,3,5], mainObject.Fibonacci(6));
    });
});

//Test case for reverse sentence by passing a sentence as a input
describe('Reverse Sentence', function(){
    it('should return the sentence after removing vowels and space series', function(){
      assert.equal('tbr,mssht', mainObject.ReverseMe('this is me, robot'));
    });
});

//Initialising two objects and passing it as an argument to the compare function
describe('Array Sorting', function(){
    it('should return 1 if id is smaller', function(){
        var obj1 = {};
        var obj2 = {};
        obj1.id = 23;
        obj2.id = 25;
      assert.equal(-1, mainObject.Compare(obj1,obj2));
        obj1.id = 2;
        obj2.id = 1;
      assert.equal(1, mainObject.Compare(obj1,obj2));
    });
});