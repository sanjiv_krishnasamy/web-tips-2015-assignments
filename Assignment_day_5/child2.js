/*global console:true, require:true, process:true */

process.stdin.resume();
process.stdin.on('data',function (data) {
    try{
        var temp;
        
        //Generating a random number
        var number = Math.floor(Math.random() * 100);
        number = number%3;
        
        //Assigning stone papper scissor depending upon the random value
        switch (parseInt(number)){
            case 0:
                temp = 'stone';
                break;
            case 1:
                temp = 'paper';
                break;
            case 2:
                temp = 'scissor';
                break;
        }
        
        //Replys to the parent
        process.stdout.write(temp);
    } catch (err) {
        process.stderr.write(err.message);
    }
});