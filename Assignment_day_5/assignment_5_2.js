/*global console:true, require:true, process:true, setInterval:true */

//Creating a parent process
var parent = require('child_process').spawn;

//Creating a Child processes
var child1 = parent('node',['child1.js']);
var child2 = parent('node',['child2.js']);

var child1Reply;
var child2Reply;

//Function to decide winners
var DecideWinner = function (child1Reply, child2Reply) {
    if (child1Reply === 'stone') {
        if (child2Reply === 'stone') {
            console.log("Match Tied....!!!!\n\n");
        } else if (child2Reply === 'paper'){
            console.log("Child2 Wins....!!!! \n\n");
        } else if (child2Reply === 'scissor') {
            console.log("Child1 Wins....!!!! \n\n");
        }
    } else if (child1Reply === 'paper') {
        if (child2Reply === 'paper') {
            console.log("Match Tied....!!!!\n\n");
        } else if (child2Reply === 'scissor'){
            console.log("Child2 Wins....!!!! \n\n");
        } else if (child2Reply === 'stone') {
            console.log("Child1 Wins....!!!! \n\n");
        }
    } else if (child1Reply === 'scissor') {
        if (child2Reply === 'scissor') {
            console.log("Match Tied....!!!!\n\n");
        } else if (child2Reply === 'stone'){
            console.log("Child2 Wins....!!!! \n\n");
        } else if (child2Reply === 'paper') {
            console.log("Child1 Wins....!!!! \n\n");
        }
    }
};
//Executes for every 3 secs
setInterval(function () {
    child1.stdin.write("Game starts");    
},3000);

//Reply from child 1
child1.stdout.on('data',function(data) {
    child1Reply = data.toString();
    console.log("Child1 choose : "+child1Reply);
    child2.stdin.write("Game Starts");
    });

//Reply from child 2
child2.stdout.on('data', function (data) {
    child2Reply = data.toString();
    console.log("Child2 choose : "+child2Reply);
    DecideWinner(child1Reply, child2Reply);
});

//If child 1 returns error
child1.stderr.on('data',function (err) {
    process.stdout.write("error"+err);
});

//If child 2 returns error
child2.stderr.on('data',function (err) {
    process.stdout.write("error"+err);
});