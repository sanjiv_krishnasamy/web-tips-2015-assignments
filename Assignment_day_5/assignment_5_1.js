/*global console:true, require:true, process:true, exports:true */
//Create a download of the following exe:- http://nodejs.org/dist/v0.12.4/node.exe. Implement it using promises. It should display the size of the download at the start of download, display the percentage complete when the download is happening, and  a task complete message after the download completes
var http = require('http');
var fs = require('fs');
var q = require('q');

//Global variables
var url = 'http://nodejs.org/dist/v0.12.4/node.exe';
var destination = 'node.exe';
var file = fs.createWriteStream(destination);

//Function to be executed after downloading
var LastFunction = function (err) {
    if(err){
        console.log("Download Failed :" +err);
    } else {
        console.log("successfully Downloaded.....!!!!");
    }
};

//Function to create a promise
var CreatePromise = function () {
    var defered = q.defer();
    http.get(url,function(response) {
        var len = parseInt(response.headers['content-length'], 10);
        var cur = 0;
        var total = len / 1048576;
        var i = 1;
        response.pipe(file);
        defered.notify("Total size :"+total.toFixed(2) + " mb");
        response.on("data", function(data) {
            cur += data.length;
            if ((100.0 * cur / len).toFixed(2) >= i * 10){
                console.log("Downloading " + (100.0 * cur / len).toFixed(2) + "% " + (cur / 1048576).toFixed(2) + " mb\r");
                i++;
            }
        });
        defered.resolve(file);
    }).on("error", function (err){
        defered.reject(err);
    });
    return defered.promise;
};

//Creating a promise with success and failure callbacks
var promise = CreatePromise();
promise.then(function (file) {
    file.on('finish', function() {
        file.close(LastFunction);  // close() is async, call LastFunction after close completes.
    });
},function (error) {
    fs.unlink(destination); // Delete the file async
    if (LastFunction) {
        LastFunction(error.message);
    }
},function (status) {
    console.log(status);
});

