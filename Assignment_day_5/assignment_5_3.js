/*global console:true, require:true, process:true, exports:true */
//Make 5_1 to use events instead of promises
var http = require('http');
var fs = require('fs');
var EventEmitter = require('events').EventEmitter;
var emitter = new EventEmitter();

//Global variables
var url = 'http://nodejs.org/dist/v0.12.4/node.exe';
var destination = 'node.exe';
var file = fs.createWriteStream(destination);

//Function to be executed after downloading
var LastFunction = function (err) {
    if(err){
        console.log("Download Failed :" +err);
    } else {
        console.log("successfully Downloaded.....!!!!");
    }
};

//Function to be called when emitter is called
emitter.on('startDownload', function (url,file) {
    var request = http.get(url, function(response) {
        var len = parseInt(response.headers['content-length'], 10);
        var cur = 0;
        var total = len / 1048576; //1048576 - bytes in  1Megabyte
        var i = 0;
        response.pipe(file);
        console.log("Total size of the file is "+total.toFixed(2)+" mb");
        
        response.on("data", function(chunk) {
            cur += chunk.length;
            if((100.0 * cur / len).toFixed(2) >= i * 10){
                console.log("Downloading "+(100.0 * cur / len).toFixed(2)+" %");
                i++;
            }
        });
        file.on('finish', function() {
            file.close(LastFunction);
        });
        request.on("error", function(e){
            fs.unlink(LastFunction);
            LastFunction(e);
        });
    });
});
emitter.emit('startDownload',url,file);
        