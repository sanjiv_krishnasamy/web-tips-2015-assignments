//Use for loop and enumerate the camera object and display in console.log
/*global console:true, exports:true*/
var camera = {
    color : 'white',
    flash : true,
    lens : 'gorilla glass',
    totalPixel : 17,
    autoFocus : true,
    TakePicture : function () {
        console.log("taking Picture.......!!!!!!!!");
    },
    SetTimer : function () {
        console.log("3....2....1....!!!!!!!!");
    }
};

//Function to print the property
var PrintProperty = function (camera) {
    var i;
    var string ='';
    for (i in camera) {
        string += i;
        string += ':';
        string += camera[i]+'\n';
    }
    return string;
};

console.log(PrintProperty(camera));

//Exporting a module for testing
exports.PrintProperty = PrintProperty;