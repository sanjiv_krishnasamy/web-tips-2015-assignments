/*global console:true, require:true, process:true, describe:true, it:true */
var assert = require("assert");
var assignment_2_2 = require("./assignment_2_2");
var assignment_2_3 = require("./assignment_2_3");
var assignment_2_5 = require("./assignment_2_5");
var assignment_2_6 = require("./assignment_2_6");
var assignment_2_7 = require("./assignment_2_7");
var assignment_2_8 = require("./assignment_2_8");
var assignment_2_11 = require("./assignment_2_11");
var assignment_2_18 = require("./assignment_2_18");

//Test case for Printing Object Property
describe('Dsiplaying the Object Property', function(){
    it('should return string displaying the property of an object', function(){
        var object = {
            name : 'camera',
            color : 'black',
            PrintMe : function () {
                console.log("Hiii...!!");
            }
        };
        var string = '';
        for (var i in object) {
            string +=i;
            string +=object[i];
        }
      assert.equal(string, assignment_2_2.PrintProperty(object));
    });
});
//Test case for printing the type of the property
describe('Dsiplaying the type of Object Property', function(){
    it('should return type of the property of an object', function(){
        var trainingRoom = {
            name : 'camera',
        };
      assert.equal('string', assignment_2_3.PrintType(trainingRoom));
    });
});

//Test case for Method Invocation
describe('Method Invocation', function(){
    it('should return the results of arithmetic operation', function(){
      assert.equal(5, assignment_2_5.operation.Sum(2,3));
      assert.equal(-1, assignment_2_5.operation.Subtract(2,3));
      assert.equal(6, assignment_2_5.operation.Multiply(2,3));
      assert.equal(2, assignment_2_5.operation.Divide(4,2));
    });
});

//Test case for Function Invocation
describe('Function Invocation', function(){
    it('should return the results of arithmetic operation', function(){
      assert.equal(5, assignment_2_6.operation.Sum(2,3));
      assert.equal(-1, assignment_2_6.operation.Subtract(2,3));
      assert.equal(6, assignment_2_6.operation.Multiply(2,3));
      assert.equal(2, assignment_2_6.operation.Divide(4,2));
    });
});

//Test case for Constructor Invocation
describe('Constructor Invocation', function(){
    it('should return the results of arithmetic operation', function(){
      assert.equal(5, new assignment_2_7.operation(2,3).Sum());
      assert.equal(-1, new assignment_2_7.operation(2,3).Subtract());
      assert.equal(6, new assignment_2_7.operation(2,3).Multiply());
      assert.equal(2, new assignment_2_7.operation(4,2).Divide());
    });
});

//Test case for Constructor Invocation
describe('Apply Invocation', function(){
    it('should return the results of arithmetic operation', function(){
      assert.equal(9, assignment_2_6.operation.Sum.apply(null,[3,6]));
      assert.equal(-2, assignment_2_6.operation.Subtract.apply(null,[5,7]));
      assert.equal(25, assignment_2_6.operation.Multiply.apply(null,[5,5]));
      assert.equal(5, assignment_2_6.operation.Divide.apply(null,[10,2]));
    });
});

//Test case for Inheritance
describe('Inheritance', function(){
    it('should return the area of the shapes', function(){
      assert.equal(16, new assignment_2_11.Square(4).FindArea());
      assert.equal(8, new assignment_2_11.Rectangle(4,2).FindArea());
      assert.equal(4, new assignment_2_11.Rhombus(4,2).FindArea());
    });
});

//Test case for Reversing a string
describe('Inheritance', function(){
    it('should return the reversed string', function(){
      assert.equal('tobor si siht', assignment_2_18.PerformOperation('this is robot'));
    });
});
