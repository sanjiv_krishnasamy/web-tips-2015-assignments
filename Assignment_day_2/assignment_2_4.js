//Create an object for soliton training room. Whatever and whoever is present at that moment should be a property of that object and assign a value which is relevant to the property
/*global console:true, require:true, process:true */
var readline = require("readline");
var count;
var trainingRoom = {};

//Creating a interface to use readline module
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//Function to get property and value and assign them to the object
function GetProperty (){
    var i;
    rl.question('Name of the item :',function(name) {
        var propertyName = name;
        rl.question('Value of the item :',function(value) {
            var propertyValue = value;
            trainingRoom[propertyName] = propertyValue;
            if(count !== 0) {
                GetProperty();
            } else {
                for (i in trainingRoom) {
                    console.log (i +" : "+trainingRoom[i]);
                }
                process.exit();
            }
        });
    });
    count--;
}

//Getting the input from the user
rl.question('How many items are present in the room right now?\n', function(answer) {
  if(!isNaN(answer)) {
      count = parseInt(answer);
      GetProperty();
  } else {
      console.log("This is not a valid number !!!");
  }
});