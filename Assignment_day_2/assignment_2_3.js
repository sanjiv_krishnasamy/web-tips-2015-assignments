//Use ‘typeof’ in the above enumeration and limit characteristics of type only ‘Number’
/*global console:true, exports:true*/

var camera = {
    color : 'white',
    flash : true,
    lens : 'gorilla glass',
    totalPixel : 17,
    autoFocus : true,
    TakePicture : function () {
        console.log("taking Picture.......!!!!!!!!");
    },
    SetTimer : function () {
        console.log("3....2....1....!!!!!!!!");
    }
};
var i;
console.log("Data type of properties in camera object :");

//Function to generate the type of the properties
var PrintType = function (camera) {
    var type = '';
    for (i in camera) {
        type += typeof(camera[i])+'\n';
    }
    return type;
};
//Printing the type of the property
console.log(PrintType (camera));

console.log("\nFollowing property in camera object has the number as their data type :");
//Loop to display the property that belongs to number type
for (i in camera) {
    if (typeof(camera[i]) === 'number') {
        console.log(i);
    }
}

exports.PrintType = PrintType;