//Implement the arithmetic module using “closure” Expose function to set their value
/*global console:true, require:true, process:true */

var readline = require("readline");
var number1;
var number2;
//Creating a interface to use readline module
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//Functions performing Arithmetic Operation
var Sum = function() {
        return parseInt(number1)+parseInt(number2);
};
var Subtract = function() {
        return number1-number2;
};
var Multiply = function() {
        return number1*number2;
};
var Divide = function() {
    if(number2 !== 0) {
        return number1/number2;
    } else {
        console.log ("Divider cannot br zero !!!");
        return 0;
    }
};

//Function to take the input from the user and displaying the arithmetic results
function GetValue () {
        rl.question('Enter number 1 :',function (temp1) {
            rl.question('Enter number 2 :',function (temp2) {
                number1 = temp1;
                number2 = temp2;
                if (!isNaN(number1) &&!isNaN(number2) && number1 !== '' && number2 !== '') {
                    console.log("Sum of two number is :"+Sum());
                    console.log("Subtraction of two number is :"+Subtract());
                    console.log("Multiplication of two number is :"+Multiply());
                    console.log("Division of two number is :"+Divide());
                    process.exit();
                } else {
                    console.log("Enter a number please !!!");
                    GetValue();
                }
            });
        });
}
GetValue();