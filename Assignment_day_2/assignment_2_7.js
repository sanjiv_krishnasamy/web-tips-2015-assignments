//Implement the above using “Constructor” invocation
/*global console:true, require:true, process:true, exports:true */

var readline = require("readline");

//Creating a interface to use readline module
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//Assinging the value using a constructor
var operation = function (number1, number2){
    this.number1 = number1;
    this.number2 = number2;
};

//Functions performing Arithmetic Operation
operation.prototype.Sum = function () {
        return parseInt(this.number1)+parseInt(this.number2);
};
operation.prototype.Subtract = function () {
        return this.number1-this.number2;
};
operation.prototype.Multiply = function () {
        return this.number1*this.number2;
};
operation.prototype.Divide = function () {
        return this.number1/this.number2;
};

//Function to take the input from the user and displaying the arithmetic results
function GetValue () {
        rl.question('Enter number 1 :',function (number1) {
            rl.question('Enter number 2 :',function (number2) {
                if (!isNaN(number1) && !isNaN(number2) && number1 !== '' && number2 !== '') {
                    console.log("Sum of two number is :"+new operation(number1,number2).Sum());
                    console.log("Subtraction of two number is :"+new operation(number1,number2).Subtract());
                    console.log("Multiplication of two number is :"+new operation(number1,number2).Multiply());
                    console.log("Division of two number is :"+new operation(number1,number2).Divide());
                    process.exit();
                } else {
                    console.log("Enter a number please !!!");
                    GetValue();
                }
            });
        });
}
GetValue();
exports.operation = operation;
