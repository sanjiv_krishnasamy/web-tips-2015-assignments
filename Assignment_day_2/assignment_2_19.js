//Write a “stack” using Arrays?
/*global console:true, require:true, process:true */
var rl = require("readline-sync");
var array = ["sam", "john", "ram"];

//Function to display the array
function display() {
    var i;
    for (i in array) {
        process.stdout.write(array[i]+" ");
    }
}
console.log("The content in the element are :");
display();

//Base function to iterate till the user choose to quit
function Operation () {
    
    //Getting the choice from the user
    var choice = rl.question('\n1.Add to stack \n2.Remove from stack \n3.Quit \nEnter your choice');
        switch (parseInt(choice)) {
            case 1:
                AddToStack();
                display();
                Operation();
                break;
            case 2:
                RemoveFromStack();
                display();
                Operation();
                break;
            case 3:
                process.exit();
                break;
            default:
                console.log("Enter a valid choice");
                Operation();
        }
    
    //Performs the PUSH
    function AddToStack () {
        var newElement = rl.question('\nEnter the element to add :');
        if (newElement.length > 0) {
            array.push(newElement);
        } else {
            console("Enter any character");
            AddToStack();
        }
    }
    
    //Performs the POP
    function RemoveFromStack () {
        array.pop();
    }
}
Operation();
