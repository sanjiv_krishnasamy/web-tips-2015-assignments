//Can you update the length property by yourself? If so then share more insights on that.
/*global console:true, require:true, process:true */
var array = ["apple", "banana", "orange", "goa", "grape", "watermelon"];
var i;
//Reducing the length of an array
array.length = 4;

//Displaying the array
for (i in array) {
    console.log(array[i]);
}

//Increasing the length of an array
array.length = 10;

//Displaying the array
for (i in array) {
    console.log(array[i]);
}