//Create arithmetic functions - add, subtract, multiply and divide which takes two ‘number’ arguments and return the result. Implement it using “Method” invocation
/*global console:true, require:true, process:true, exports:true */

var readline = require("readline");

//Creating a interface to use readline module
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//Object Operation cntaining the basic arithmetic operation
var operation = {
    Sum : function (number1, number2) {
        return parseInt(number1)+parseInt(number2);
    },
    Subtract : function (number1, number2) {
        return number1-number2;
    },
    Multiply : function (number1, number2) {
        return number1*number2;
    },
    Divide : function (number1, number2) {
            return number1/number2;
    }
};

//Function to take the input from the user and displaying the arithmetic results
function GetValue () {
        rl.question('Enter number 1 :',function (number1) {
            rl.question('Enter number 2 :',function (number2) {
                    if (!isNaN(number1) &&!isNaN(number2) && number1 !== '' && number2 !== '') {
                        console.log("Sum of two number is :"+operation.Sum(number1,number2));
                        console.log("Subtraction of two number is :"+operation.Subtract(number1,number2));
                        console.log("Multiplication of two number is :"+operation.Multiply(number1,number2));
                        console.log("Division of two number is :"+operation.Divide(number1,number2));
                        process.exit();
                    } else {
                        console.log("Enter a number please !!!");
                        GetValue();
                    }
                } catch (e) {
                    console.log(e);
                    
                }
            });
        });
}
GetValue();

exports.operation = operation;