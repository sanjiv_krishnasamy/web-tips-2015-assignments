//Reverse the string “This is TIPS 2015” using reverse function.
/*global console:true, require:true, process:true, exports:true */

var string = "This is TIPS 2015";

var PerformOperation = function (string) {
    //Reversing and joining the array
    return string.split('').reverse().join('');
};


var reversedString = PerformOperation(string);

//Displaying the string
console.log(reversedString);

exports.PerformOperation = PerformOperation;