//Describe camera as an object and define some of its characteristics and operations
/*global console:true*/
var camera = {
    color : 'white',
    flash : true,
    lens : 'gorilla glass',
    totalPixel : 17,
    autoFocus : true,
    TakePicture : function () {
        console.log("taking Picture.......!!!!!!!!");
    },
    SetTimer : function () {
        console.log("3....2....1....!!!!!!!!");
    }
};