//Use for loop and enumerate through your items and print in console for the above two arrays
/*global console:true, require:true, process:true */

var interestingThings = ["Movies","Cricket","Football","Gaming","Hiking"];
var i;
//Displaying the array
for (i in interestingThings) {
    console.log(interestingThings[i]);
}