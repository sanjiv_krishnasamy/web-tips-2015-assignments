//Splice and slice
/*global console:true, require:true, process:true */

var array1 = ["apple", "banana", "orange", "goa", "grape", "watermelon"];
var array2 = ["apple", "banana", "orange", "goa", "grape", "watermelon"];
var i;
console.log("After Splicing ");
//Splicing the array
array1.splice (2,3);

//Displaying the array
for (i in array1) {
    console.log(array1[i]);
}

console.log("\nAfter Slicing ");
//Slicing the array
var array3 = array2.slice (1,3);

//Displaying the array
for (i in array3) {
    console.log(array3[i]);
}
