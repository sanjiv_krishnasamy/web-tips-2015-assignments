//Line, Rectangle, Square and Rhombus. Provide an inheritance relation based on you feel (Should be multiple and multi level) and do calculate Area.
/*global console:true, require:true, process:true, exports:true */

//Super parent
var Shape = function (name) {
    var that = {};
    that.name = name;
    that.DescribeMe = function () {
        console.log("Hi...!! I am "+that.name);
    };
    return that;
};

//Parent
var Line1 = function (length,name) {
    var that = new Shape (name);
    that.length = length;
    return that;
};

//Parent
var Line2 = function () {
    this.breadth=0;
};

Line2.prototype.get_breadth = function(breadth){
    this.breadth = breadth;
};

//Multilevel Inheritance using functional implementation || Child
var Square = function (len) {
    var that = new Line1 (len);
    that.name = 'square';
    that.FindArea = function () {
        return (that.length * that.length);
    };
    return that;
};

//Multiple Inheritance using functional & pseudoclassical implementation || Child
var Rectangle = function (length, breadth) {
    var that = new Line1 (length);
    that.prototype = new Line2();
    that.prototype.get_breadth(breadth);
    that.name = 'Rectangle';
    that.FindArea = function () {
        return (that.length * that.prototype.breadth);
    };
    return that;
};

//Multiple Inheritance using functional & prototypal implementation || Child
var Rhombus = function (diagonal1, diagonal2) {
    var that = Object.create(Line1(diagonal1,'Rhombus'), {breadth :{value : diagonal2}});
    that.FindArea = function () {
        return (that.length * that.breadth / 2);
    };
    return that;
};
//Initialising the objects
var square = new Square (4);
var rectangle = new Rectangle (4,2);
var rhombus = new Rhombus (4,2);

//Calling the function
square.DescribeMe();
console.log("(Using Functional Inheritance) My Area is : "+square.FindArea());
rectangle.DescribeMe();
console.log("(Using Functional and Pseudoclassical Inheritance)My Area is : "+rectangle.FindArea());
rhombus.DescribeMe();
console.log("(Using Functional and prototypal Inheritance)My Area is : "+rhombus.FindArea());

exports.Square = Square;
exports.Rectangle = Rectangle;
exports.Rhombus = Rhombus;