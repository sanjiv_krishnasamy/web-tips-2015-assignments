//Extend the Number, String functionality with your own functions. Choose one or two functions for each type. Use functional inheritance
/*global console:true, require:true, process:true */

//Overwriting Number
Number.prototype.show = function(){
    console.log("This is a number!!");   
};

//Overwriting String
String.prototype.show = function(){
    console.log("This is a String!!");   
};

//Declaring a number and calling a show function
var num1 = 8;
num1.show();

//Declaring a string and calling a show function
var name = 'sanjiv';
name.show();
