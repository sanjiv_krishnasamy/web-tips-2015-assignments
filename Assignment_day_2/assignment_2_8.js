//Implement the above using “apply” invocation
/*global console:true, require:true, process:true, exports:true */

var readline = require("readline");

//Creating a interface to use readline module
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//Functions performing Arithmetic Operation
var Sum = function() {
        return parseInt(arguments[0])+parseInt(arguments[1]);
};
var Subtract = function() {
        return parseInt(arguments[0])-parseInt(arguments[1]);
};
var Multiply = function() {
        return parseInt(arguments[0])*parseInt(arguments[1]);
};
var Divide = function() {
    if(parseInt(arguments[1]) !== 0) {
        return parseInt(arguments[0])/parseInt(arguments[1]);
    } else {
        console.log ("Divider cannot br zero !!!");
        return 0;
    }
};


//Function to take the input from the user and displaying the arithmetic results
function GetValue () {
        rl.question('Enter number 1 :',function (number1) {
            rl.question('Enter number 2 :',function (number2) {
                if (!isNaN(number1) &&!isNaN(number2) && number1 !== '' && number2 !== '') {
                    console.log("Sum of two number is :"+Sum.apply(null,[number1,number2]));
                    console.log("Subtraction of two number is :"+Subtract.apply(null,[number1,number2]));
                    console.log("Multiplication of two number is :"+Multiply.apply(null,[number1,number2]));
                    console.log("Division of two number is :"+Divide.apply(null,[number1,number2]));
                    process.exit();
                } else {
                    console.log("Enter a number please !!!");
                    GetValue();
                }
            });
        });
}
GetValue();
var operation = {};
operation.Sum = Sum;
operation.Subtract = Subtract;
operation.Multiply = Multiply;
operation.Divide = Divide;
exports.operation = operation;